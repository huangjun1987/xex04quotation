﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace XEx04Quotation
{
    public partial class Confirm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
            var salesPrice = (string)Session["salesPrice"];
            if (!string.IsNullOrWhiteSpace(salesPrice))
            {
                lblSalesPrice.Text = (string)Session["salesPrice"];
                lblDiscountAmount.Text = (string)Session["discountAmount"];
                lblTotalPrice.Text = (string)Session["totalPrice"];
            }

        }
        protected void btnSendQuotation_Click(object sender, EventArgs e)
        {
            lblMessage.Text = $"Quotation sent to {txtName.Text} at {txtEmail.Text}.";
        }
        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
    }
}